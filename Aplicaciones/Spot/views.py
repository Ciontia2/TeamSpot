from django.shortcuts import render, redirect
from django.core.files.storage import default_storage
from django.db.models.deletion import ProtectedError
from django.conf import settings
from django.contrib import messages
from .models import Departamento
from .models import Puesto
from .models import Empleado
from .models import Asistencia
from .models import Vacaciones
from .models import Pago
from django.http import HttpResponseRedirect
from django.core.mail import send_mail
# Create your views here.


def listadoDepartamentos(request):
    departamentosBdd = Departamento.objects.all()
    return render(request, 'listadoDepartamentos.html', {'departamentos': departamentosBdd})

def guardarDepartamento(request):
    if request.method == 'POST':
        nombre_departamento = request.POST.get("nombre_departamento")
        descripcion_departamento = request.POST.get("descripcion_departamento")
        direccion_departamento = request.POST.get("direccion_departamento")
        logo_departamento = request.FILES.get("logo_departamento")

        departamento = Departamento.objects.create(
            nombre_de=nombre_departamento,
            descripcion_de=descripcion_departamento,
            direccion_de=direccion_departamento,
            logo_de=logo_departamento
        )

        messages.success(request, 'DEPARTAMENTO GUARDADO EXITOSAMENTE')
        return redirect('/')

def eliminarDepartamento(request, id_departamento):
    try:
        departamento_eliminar = Departamento.objects.get(id_de=id_departamento)
        try:
            departamento_eliminar.delete()
            messages.success(request, 'DEPARTAMENTO ELIMINADO EXITOSAMENTE')
        except ProtectedError:
            # Si hay objetos relacionados protegidos, mostrar una notificación al usuario
            messages.warning(request, 'No se puede eliminar el departamento porque existen puestos relacionados.')
    except Departamento.DoesNotExist:
        messages.error(request, 'El departamento no existe')

    return redirect('/')

def editarDepartamento(request, id_departamento):
    try:
        departamento_editar = Departamento.objects.get(id_de=id_departamento)
        departamentos_bdd = Departamento.objects.all()
        return render(request, 'editarDepartamento.html', {'departamento': departamento_editar, 'departamentos': departamentos_bdd})
    except Departamento.DoesNotExist:
        messages.error(request, 'El departamento no existe')
        return redirect('/')

def procesarActualizacionDepartamento(request):
    if request.method == 'POST':
        id_departamento = request.POST.get("id_departamento")
        nombre_departamento = request.POST.get("nombre_departamento")
        descripcion_departamento = request.POST.get("descripcion_departamento")
        direccion_departamento = request.POST.get("direccion_departamento")
        logo_de = request.FILES.get('logo_de')

        try:
            departamento_editar = Departamento.objects.get(id_de=id_departamento)

            if logo_de is not None:
                departamento_editar.logo_de = logo_de

            departamento_editar.nombre_de = nombre_departamento
            departamento_editar.descripcion_de = descripcion_departamento
            departamento_editar.direccion_de = direccion_departamento
            departamento_editar.save()

            messages.success(request, 'DEPARTAMENTO ACTUALIZADO EXITOSAMENTE')
        except Departamento.DoesNotExist:
            messages.error(request, 'El departamento no existe')

    return redirect('/')


#puesto-----------------------------------------------------------------------------------------------
def listadoPuestos(request):
    puestosBdd = Puesto.objects.all()
    departamentosBdd = Departamento.objects.all()

    return render(request, 'listadoPuestos.html', {'puestos': puestosBdd, 'departamentos': departamentosBdd})

def guardarPuesto(request):
    id_departamento = request.POST["id_departamento"]
    departamentoSeleccionada = Departamento.objects.get(id_de=id_departamento)

    nombre_pu = request.POST["nombre_pu"]
    responsabilidades_pu = request.POST["responsabilidades_pu"]
    salario_base_pu= request.POST["salario_base_pu"]
    nuevoPuesto = Puesto.objects.create(
        nombre_pu = nombre_pu,
        responsabilidades_pu = responsabilidades_pu,
        salario_base_pu = salario_base_pu,
        departamento = departamentoSeleccionada
    )

    messages.success(request, 'Puesto Guardado Exitosamente')
    return redirect('/listadoPuestos')

def eliminarPuesto(request, id_pu):
    try:
        puestoEliminar = Puesto.objects.get(id_pu=id_pu)
        puestoEliminar.delete()
        messages.success(request, 'Puesto Eliminado Exitosamente')
    except ProtectedError:
        messages.error(request, 'No se puede eliminar el puesto porque tiene referencias protegidas en empleado.')
    return redirect('/listadoPuestos')

def editarPuesto(request, id):
    puestoEditar = Puesto.objects.get(id_pu=id)
    departamentosBdd = Departamento.objects.all()  # Nombre de variable corregido
    return render(request, 'editarPuesto.html', {'puesto': puestoEditar, 'departamentos': departamentosBdd})

def procesarActualizacionPuesto(request):
    id_pu = request.POST["id_pu"]
    id_departamento = request.POST["id_departamento"]
    departamentoSeleccionada = Departamento.objects.get(id_de=id_departamento)
    nombre_pu = request.POST["nombre_pu"]
    responsabilidades_pu = request.POST["responsabilidades_pu"]
    salario_base_pu= request.POST["salario_base_pu"]


    # Actualizando datos mediante el ORM de Django
    puestoEditar = Puesto.objects.get(id_pu=id_pu)
    puestoEditar.departamentos = departamentoSeleccionada  # Corrección aquí
    puestoEditar.nombre_pu = nombre_pu
    puestoEditar.responsabilidades_pu= responsabilidades_pu
    puestoEditar.salario_base_pu = salario_base_pu
    puestoEditar.save()

    messages.success(request, 'PUESTOACTUALIZADO EXITOSAMENTE')
    return redirect('/listadoPuestos')


#Empleado----------------------------------------------------------------------------------------
def listadoEmpleados(request):
    empleadosBdd = Empleado.objects.all()
    puestosBdd = Puesto.objects.all()

    return render(request, 'listadoEmpleados.html', {'empleados': empleadosBdd, 'puestos': puestosBdd})

 # Asegúrate de importar el modelo correcto

def guardarEmpleado(request):
        id_puesto = request.POST["empleado"]
        puestoSeleccionada = Puesto.objects.get(id_pu=id_puesto)
        cedula_em = request.POST["cedula_em"]
        apellido_em = request.POST["apellido_em"]
        nombre_em = request.POST["nombre_em"]
        fecha_contratacion_em = request.POST["fecha_contratacion_em"]
        telefono_em = request.POST["telefono_em"]
        correo_electronico_em = request.POST["correo_electronico_em"]
        genero_em = request.POST["genero_em"]
        foto_em = request.FILES.get("foto_em")
        nuevoEmpleado = Empleado.objects.create(
            cedula_em=cedula_em,
            apellido_em=apellido_em,
            nombre_em=nombre_em,
            fecha_contratacion_em=fecha_contratacion_em,
            telefono_em=telefono_em,
            correo_electronico_em=correo_electronico_em,
            genero_em=genero_em,
            foto_em=foto_em,
            puesto=puestoSeleccionada
        )

        messages.success(request, 'Empleado Guardado Exitosamente')
        return redirect('/listadoEmpleados')
#empleados----------------------------------------------------------------------------------------------------------
def eliminarEmpleado(request, id_em):
    try:
        empleadoeliminar = Empleado.objects.get(id_em=id_em)
        empleadoeliminar.delete()
        messages.success(request, 'Empleado eliminado exitosamente')
    except ProtectedError:
        messages.error(request, 'No se puede eliminar el empleado porque tiene referencias protegidas .')
    return redirect('/listadoEmpleados')


def editarEmpleado(request, id):
    empleadoEditar = Empleado.objects.get(id_em=id)
    puestosBdd = Puesto.objects.all()  # Suponiendo que Puestos es el modelo correcto
    return render(request, 'editarEmpleado.html', {'empleado': empleadoEditar, 'puestos': puestosBdd})

def procesarActualizacionEmpleado(request):

    if request.method == 'POST':
        id_empleado = request.POST["id_em"]
        id_puesto = request.POST["id_puesto"]
        puestoSeleccionada = Puesto.objects.get(id_pu=id_puesto)
        cedula_em = request.POST["cedula_em"]
        apellido_em = request.POST["apellido_em"]
        nombre_em = request.POST["nombre_em"]
        fecha_contratacion_em = request.POST["fecha_contratacion_em"]
        telefono_em = request.POST["telefono_em"]
        correo_electronico_em = request.POST["correo_electronico_em"]
        genero_em = request.POST["genero_em"]
        foto_em = request.FILES.get('foto_em')

        try:
            empleadoEditar = Empleado.objects.get(id_em=id_empleado)

            if foto_em is not None:
                empleadoEditar.foto_em = foto_em

            empleadoEditar.puesto = puestoSeleccionada  # Corrección aquí
            empleadoEditar.cedula_em = cedula_em
            empleadoEditar.nombre_em = nombre_em
            empleadoEditar.fecha_contratacion_em = fecha_contratacion_em
            empleadoEditar.telefono_em = telefono_em
            empleadoEditar.correo_electronico_em = correo_electronico_em
            empleadoEditar.save()
            messages.success(request, 'EMPLEADO ACTUALIZADO EXITOSAMENTE')
        except Empleado.DoesNotExist:
            messages.error(request, 'El departamento no existe')

    return redirect('/listadoEmpleados')

    #ASISTENCIA---------------------------------------------------------------------------------------------
def listadoAsistencias(request):
    asistenciasBdd = Asistencia.objects.all()
    empleadosBdd = Empleado.objects.all()
    return render(request, 'listadoAsistencias.html', {'asistencias': asistenciasBdd, 'empleados': empleadosBdd})

def eliminarAsistencia(request, id_as):
    try:
        asistenciaEliminar = Asistencia.objects.get(id_as=id_as)
        asistenciaEliminar.delete()
        messages.success(request, 'Asistencia eliminada exitosamente')
    except Asistencia.DoesNotExist:
        messages.error(request, 'La asistencia no existe')
    return redirect('/listadoAsistencias/')

def guardarAsistencia(request):

    id_empleado = request.POST["id_empleado"]
    empleadoSeleccionada = Empleado.objects.get(id_em=id_empleado)
    fecha_as = request.POST["fecha_as"]
    hora_entrada_as = request.POST["hora_entrada_as"]
    hora_salida_as = request.POST["hora_salida_as"]
    hoja_as = request.FILES.get("hoja_as")
    estado_as = request.POST["estado_as"]
    # Inserting data using Django's ORM
    asistencia = Asistencia.objects.create(
        empleado=empleadoSeleccionada,
        fecha_as=fecha_as,
        hora_entrada_as=hora_entrada_as,
        hora_salida_as=hora_salida_as,
        hoja_as=hoja_as,
        estado_as=estado_as
    )

    # Verifica si 'empleado' está presente en request.POST
    if 'id_empleado' in request.POST:
        empleado = request.POST['id_empleado']
        empleadoSeleccionada = Empleado.objects.get(id_em=id_empleado)
        fecha_as = request.POST['fecha_as']
        hora_entrada_as = request.POST['hora_entrada_as']
        hora_salida_as = request.POST['hora_salida_as']
        hoja_as = request.FILES.get('hoja_as', None)
        estado_as = request.POST['estado_as']

        # Insertar datos usando el ORM de Django
        asistencia = Asistencia.objects.create(
            empleado=empleadoSeleccionada,
            fecha_as=fecha_as,
            hora_entrada_as=hora_entrada_as,
            hora_salida_as=hora_salida_as,
            hoja_as=hoja_as,
            estado_as=estado_as
        )

        messages.success(request, 'Asistencia guardada exitosamente')
        return redirect('/listadoAsistencias/')
    else:
        # Manejar el caso en el que 'empleado' no está presente en request.POST
        messages.error(request, 'Error al guardar la asistencia. Asegúrate de enviar todos los campos requeridos.')
        return redirect('/listadoAsistencias/')  # Puedes redirigir a una página de error o a donde desees



def editarAsistencia(request, id_as):
    asistenciasBdd = Asistencia.objects.get(id_as=id_as)
    empleadosBdd = Empleado.objects.all()
    return render(request, 'editarAsistencia.html', {'asistencias': asistenciasBdd, 'empleados': empleadosBdd})


def procesarActualizacionAsistencia(request):
    if request.method == 'POST':
        id_as=request.POST["id_as"]
        id_empleado = request.POST["id_empleado"]
        empleadoSeleccionada = Empleado.objects.get(id_em=id_empleado)
        fecha_as = request.POST["fecha_as"]
        hora_entrada_as = request.POST["hora_entrada_as"]
        hora_salida_as = request.POST["hora_salida_as"]
        hoja_as = request.FILES.get("hoja_as")
        estado_as = request.POST["estado_as"]
        try:
            asistenciaEditar = Asistencia.objects.get(id_as=id_as)

            if hoja_as is not None:
                asistenciaEditar.hoja_as = hoja_as
    # Update the asistencia with the new data
            asistenciaEditar.empleado = empleadoSeleccionada
            asistenciaEditar.fecha_as = fecha_as
            asistenciaEditar.hora_entrada_as = hora_entrada_as
            asistenciaEditar.hora_salida_as = hora_salida_as
            asistenciaEditar.estado_as=estado_as
            asistenciaEditar.save()

            messages.success(request, 'Asistencia actualizada exitosamente')
        except Asistencia.DoesNotExist:
            messages.error(request, 'La asistencia no existe')

    return redirect('/listadoAsistencias/')

#Vacaciones--------------------------------------------------------------------------------------
def listadoVacaciones(request):
    vacacionesBdd = Vacaciones.objects.all()
    empleadosBdd = Empleado.objects.all()
    return render(request, 'listadoVacaciones.html', {'vacaciones': vacacionesBdd, 'empleados': empleadosBdd})




def eliminarVacaciones(request, id_va):
    try:
        vacacionEliminar = Vacaciones.objects.get(id_va=id_va)
        vacacionEliminar.delete()
        messages.success(request, 'Vacación eliminada exitosamente')
    except Vacaciones.DoesNotExist:
        messages.error(request, 'La vacación no existe')
    return redirect('/listadoVacaciones/')

def guardarVacaciones(request):
    empleado=request.POST["empleado"]
    empleadoSeleccionada = Empleado.objects.get(id_em=empleado)
    fecha_inicio_va = request.POST["fecha_inicio_va"]
    fecha_fin_va = request.POST["fecha_fin_va"]
    estado_solicitud_va = request.POST["estado_solicitud_va"]
    motivo_rechazo_va = request.POST["motivo_rechazo_va"]

    # Insertando datos mediante el ORM de Django
    vacacion = Vacaciones.objects.create(
        fecha_inicio_va=fecha_inicio_va,
        fecha_fin_va=fecha_fin_va,
        estado_solicitud_va=estado_solicitud_va,
        motivo_rechazo_va=motivo_rechazo_va,
        empleado=empleadoSeleccionada
    )

    messages.success(request, 'Vacación guardada exitosamente')
    return redirect('/listadoVacaciones/')






def editarVacaciones(request, id_va):
    vacacionesEditar = Vacaciones.objects.get(id_va=id_va)
    empleadosBdd = Empleado.objects.all()
    return render(request, 'editarVacacion.html', {'vacaciones': vacacionesEditar,'empleados': empleadosBdd})

def procesarActualizacionVacaciones(request):

    id_va = request.POST["id_va"]
    id_empleado=request.POST["id_empleado"]
    empleadoSeleccionada = Empleado.objects.get(id_em=id_empleado)

    fecha_inicio_va = request.POST["fecha_inicio_va"]
    fecha_fin_va = request.POST["fecha_fin_va"]
    estado_solicitud_va = request.POST["estado_solicitud_va"]
    motivo_rechazo_va = request.POST["motivo_rechazo_va"]


    vacacionEditar = Vacaciones.objects.get(id_va=id_va)
    vacacionEditar.empleado = empleadoSeleccionada  # Corrección aquí
    vacacionEditar.fecha_inicio_va = fecha_inicio_va
    vacacionEditar.fecha_fin_va = fecha_fin_va
    vacacionEditar.estado_solicitud_va = estado_solicitud_va
    vacacionEditar.motivo_rechazo_va = motivo_rechazo_va

    vacacionEditar.save()

    messages.success(request, 'Vacación actualizada exitosamente')
    return redirect('/listadoVacaciones/')



#Pagos-------------------------------------------------------------------------------------------------------------------------

def listadoPagos(request):
    pagosBdd = Pago.objects.all()
    empleadosBdd = Empleado.objects.all()

    return render(request, 'listadoPagos.html', {'pagos': pagosBdd, 'empleados': empleadosBdd})

def guardarPago(request):
    id_empleado = request.POST["id_empleado"]
    empleadoSeleccionada = Empleado.objects.get(id_em=id_empleado)

    fecha_pago_pa = request.POST["fecha_pago_pa"]
    periodo_pago_pa = request.POST["periodo_pago_pa"]
    horas_extra_pa= request.POST["horas_extra_pa"]
    nuevoPago = Pago.objects.create(
        fecha_pago_pa = fecha_pago_pa,
        periodo_pago_pa = periodo_pago_pa,
        horas_extra_pa = horas_extra_pa,
        empleado = empleadoSeleccionada
    )

    messages.success(request, 'Pago Guardado Exitosamente')
    return redirect('/listadoPagos')

def eliminarPago(request, id_pa):
    try:
        pagoEliminar = Pago.objects.get(id_pa=id_pa)
        pagoEliminar.delete()
        messages.success(request, 'Pago Eliminado Exitosamente')
    except ProtectedError:
        messages.error(request, 'No se puede eliminar el pago porque tiene referencias protegidas en empleado.')
    return redirect('/listadoPagos')

def editarPago(request, id):
    pagoEditar = Pago.objects.get(id_pa=id)
    empleadosBdd = Empleado.objects.all()  # Nombre de variable corregido
    return render(request, 'editarPago.html', {'pago': pagoEditar, 'empleados': empleadosBdd})

def procesarActualizacionPago(request):

    id_pa = request.POST["id_pa"]
    id_empleado = request.POST["id_empleado"]
    empleadoSeleccionada = Empleado.objects.get(id_em=id_empleado)

    fecha_pago_pa = request.POST["fecha_pago_pa"]
    periodo_pago_pa = request.POST["periodo_pago_pa"]
    horas_extra_pa= request.POST["horas_extra_pa"]

    # Actualizando datos mediante el ORM de Django
    pagoEditar = Pago.objects.get(id_pa=id_pa)
    pagoEditar.empleados = empleadoSeleccionada  # Corrección aquí
    pagoEditar.fecha_pago_pa = fecha_pago_pa
    pagoEditar.periodo_pago_pa= periodo_pago_pa
    pagoEditar.horas_extra_pa = horas_extra_pa
    pagoEditar.save()

    messages.success(request, 'PAGO ACTUALIZADO EXITOSAMENTE')
    return redirect('/listadoPagos')

#enviar_correo ----------------------------------------------------------------------------------------------------------
def vistal(request):
    return render(request, 'enviar_correo.html')

def enviar_correo(request):
    if request.method == 'POST':
        destinatario = request.POST.get('destinatario')
        asunto = request.POST.get('asunto')
        cuerpo = request.POST.get('cuerpo')

        send_mail(asunto, cuerpo, settings.EMAIL_HOST_USER, [destinatario], fail_silently=False)
        messages.success(request, 'Se ha enviado tu correo ')
        return HttpResponseRedirect('/')  # Puedes redirigir a una página de éxito

    return render(request, 'enviar_correo.html')
