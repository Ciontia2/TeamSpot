from django.contrib import admin
from .models import Departamento, Puesto, Empleado, Asistencia, Vacaciones, Pago
# Register your models here.
admin.site.register(Departamento)
admin.site.register(Puesto)
admin.site.register(Empleado)
admin.site.register(Asistencia)
admin.site.register(Vacaciones)
admin.site.register(Pago)
