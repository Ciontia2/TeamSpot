from django.urls import path
from . import views


urlpatterns = [
    path('', views.listadoDepartamentos),
    path('guardarDepartamento/', views.guardarDepartamento),
    path('eliminarDepartamento/<int:id_departamento>/', views.eliminarDepartamento),
    path('editarDepartamento/<int:id_departamento>/', views.editarDepartamento),
    path('procesarActualizacionDepartamento/', views.procesarActualizacionDepartamento),
#puesto-------------------------------------------------------------------------------------
    path('listadoPuestos/', views.listadoPuestos, name='listado_puestos'),
    path('listadoPuestos/guardarPuesto/', views.guardarPuesto, name='guardar_puesto'),
    path('eliminarPuesto/<id_pu>', views.eliminarPuesto, name='eliminar_puesto'),
    path('editarPuesto/<id>', views.editarPuesto, name='editar_puesto'),
    path('procesarActualizacionPuesto/', views.procesarActualizacionPuesto, name='procesar_actualizacion_puesto'),
#Emṕleado------------------------------------------------------------------------------------------------------------
    path('listadoEmpleados/', views.listadoEmpleados, name='listado_empleados'),
    path('listadoEmpleados/guardarEmpleado/', views.guardarEmpleado, name='guardar_empleado'),
    path('eliminarEmpleado/<id_em>', views.eliminarEmpleado, name='eliminar_empleado'),
    path('editarEmpleado/<id>', views.editarEmpleado, name='editar_empleado'),
    path('procesarActualizacionEmpleado/', views.procesarActualizacionEmpleado, name='procesar_actualizacion_empleado'),
#Asistencia--------------------------------------------------------------------------------------------------------
    path('listadoAsistencias/', views.listadoAsistencias),
    path('guardarAsistencia/', views.guardarAsistencia),
    path('eliminarAsistencia/<id_as>/', views.eliminarAsistencia),
    path('editarAsistencia/<id_as>/', views.editarAsistencia),
    path('procesarActualizacionAsistencia/', views.procesarActualizacionAsistencia),

#Vacaciones-------------------------------------------------------------------------------

    path('listadoVacaciones/', views.listadoVacaciones),
    path('guardarVacaciones/', views.guardarVacaciones),
    path('eliminarVacaciones/<id_va>/', views.eliminarVacaciones),
    path('editarVacaciones/<id_va>/', views.editarVacaciones),
    path('procesarActualizacionVacaciones/', views.procesarActualizacionVacaciones),
#Pago------------------------------------------------------------------------------------------------------------------------------------------
    path('listadoPagos/', views.listadoPagos, name='listado_pagos'),
    path('listadoPagos/guardarPago/', views.guardarPago, name='guardar_pago'),
    path('eliminarPago/<id_pa>', views.eliminarPago, name='eliminar_pago'),
    path('editarPago/<id>', views.editarPago, name='editar_pago'),
    path('procesarActualizacionPago/', views.procesarActualizacionPago, name='procesar_actualizacion_pago'),
#vies para correo_electronico
    path('vistal/', views.vistal, name='vistal'),
    path('enviar_correo/',views.enviar_correo, name='enviar_correo'),
]
