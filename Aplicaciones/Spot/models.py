from django.db import models
# Create your models here.
class Departamento(models.Model):
    id_de = models.AutoField(primary_key=True)
    nombre_de = models.CharField(max_length=100)
    descripcion_de = models.TextField()
    direccion_de = models.TextField()
    logo_de  = models.FileField(upload_to='departamento', blank=True, null=True)
    def __str__(self):
        fila = "{0}: {1}"
        return fila.format(self.id_de, self.nombre_de)

class Puesto(models.Model):
    id_pu = models.AutoField(primary_key=True)
    nombre_pu = models.CharField(max_length=100)
    responsabilidades_pu = models.TextField()
    salario_base_pu = models.DecimalField(max_digits=10, decimal_places=2)
    departamento = models.ForeignKey(Departamento, on_delete=models.PROTECT, null=True, blank=True)
    def __str__(self):
        fila = "{0}: {1}"
        return fila.format(self.id_pu, self.nombre_pu)

class Empleado(models.Model):
    id_em = models.AutoField(primary_key=True)
    cedula_em = models.CharField(max_length=10)
    apellido_em = models.CharField(max_length=100)
    nombre_em = models.CharField(max_length=100)
    fecha_contratacion_em = models.DateField()
    telefono_em = models.CharField(max_length=15)
    correo_electronico_em = models.EmailField()
    genero_em = models.CharField(max_length=10, choices=[('M', 'Masculino'), ('F', 'Femenino')])
    foto_em  = models.FileField(upload_to='empleado', blank=True, null=True)
    puesto = models.ForeignKey(Puesto, on_delete=models.PROTECT, null=True, blank=True)
    def __str__(self):
        fila = "{0}: {1}"
        return fila.format(self.id_em, self.cedula_em)

class Asistencia(models.Model):
    id_as = models.AutoField(primary_key=True)
    empleado = models.ForeignKey(Empleado, on_delete=models.PROTECT, null=True, blank=True)
    fecha_as = models.DateField()
    hora_entrada_as = models.TimeField()
    hora_salida_as = models.TimeField()
    hoja_as = models.FileField(upload_to='asistencia', blank=True, null=True)
    estado_as = models.CharField(max_length=20, choices=[('Presente', 'Presente'), ('Ausente', 'Ausente'), ('Tardanza', 'Tardanza')])
    def __str__(self):
        fila = "{0}: {1}"
        return fila.format(self.id_as, self.fecha_as)

class Vacaciones(models.Model):
    id_va = models.AutoField(primary_key=True)
    empleado = models.ForeignKey(Empleado, on_delete=models.PROTECT, null=True, blank=True)
    fecha_inicio_va = models.DateField()
    fecha_fin_va = models.DateField()
    estado_solicitud_va = models.CharField(max_length=20, choices=[('Pendiente', 'Pendiente'), ('Aprobada', 'Aprobada'), ('Rechazada', 'Rechazada')])
    motivo_rechazo_va = models.TextField()
    def __str__(self):
        fila = "{0}: {1}"
        return fila.format(self.id_va, self.fecha_va)

class Pago(models.Model):
    id_pa = models.AutoField(primary_key=True)
    empleado = models.ForeignKey(Empleado, on_delete=models.PROTECT, null=True, blank=True)
    fecha_pago_pa = models.DateField()
    periodo_pago_pa = models.CharField(max_length=20, choices=[('Mensual', 'Mensual'), ('Quincenal', 'Quincenal')])
    horas_extra_pa = models.CharField(max_length=100)
    def __str__(self):
        fila = "{0}: {1}"
        return fila.format(self.id_pa, self.fecha_pago_pa)
    # Otros campos relacionados con el salario
